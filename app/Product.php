<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['code', 'name', 'short_name','brand_id', 'remark', 'order', 'status'];

    public function brand(){
        return $this->belongsTo('App\Brand');
    }
}
