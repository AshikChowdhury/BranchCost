<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankCharge extends Model
{
    protected $table = 'bank_charges_summary';

    protected $dates = [
        'trans_date'
    ];

}
