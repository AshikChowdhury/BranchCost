<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankChargeDetail extends Model
{
    protected $table = 'bank_charge_details';

    protected $dates = [
        'trans_date'
    ];

    public function bank_transaction(){
       return $this->hasMany('App\BankCharge', 'trans_id', 'trans_id');
    }
}
