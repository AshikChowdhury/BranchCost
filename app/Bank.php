<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = ['bank_name','short_name','status','address','remark'];

    public function branches(){
        return $this->belongsToMany('App\Branch');
    }
}
