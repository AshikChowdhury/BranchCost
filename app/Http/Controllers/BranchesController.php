<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Branch;
use App\Product;
use Illuminate\Http\Request;

class BranchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::all();

        return view('admin.branch.index',compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banks = Bank::pluck('short_name', 'id')->all();

        $products = Product::pluck('short_name', 'id')->all();

        return view('admin.branch.create',compact('banks','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $branch = Branch::create($request->all());

        $branch->banks()->attach($request->input('banks'));

        if (array_first($request->input('products')) == 0){
            $products = Product::pluck('id')->all();
            $branch->products()->sync($products);
        }else{
            $branch->products()->sync($request->input('products'));
        }

        return redirect('/admin/branch')->with('success','Branch saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::findOrFail($id);

        $banks = Bank::pluck('short_name', 'id')->all();

        $products = Product::pluck('short_name', 'id')->all();

        return view('admin.branch.edit',compact('branch','banks','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd(array_first($request->input('products')));
        $branch = Branch::find($id);

        $branch->update($request->all());

        $branch->banks()->sync($request->input('banks'));

        if (array_first($request->input('products')) == 0){
            $products = Product::pluck('id')->all();
            $branch->products()->sync($products);
        }else{
            $branch->products()->sync($request->input('products'));
        }
        return redirect('/admin/branch/')->with('success','Branch updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Branch::findOrFail($id)->delete();

        return redirect('/admin/branch/')->with('danger', 'Branch has been deleted.');
    }
    public function status(Request $request, $id){

        $branch = Branch::find($id);

        $branch->update($request->all());

        return redirect('/admin/branch/')->with('success', 'Status updated successfully');
    }
}
