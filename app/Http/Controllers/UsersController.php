<?php

namespace App\Http\Controllers;

use App\Branch;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Log;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','id')->all();

        $branches = Branch::pluck('name', 'id')->all();
        Log::notice('New user has created');
        return view('admin.users.create', compact('roles','branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $this->validate($request, [
            'username' => 'unique:users',
            'email' => 'unique:users',
        ]);

        $input = $request->all();
        $input['password'] = bcrypt($request->password);
//        $role = $request->role;
        $user = User::create($input);

        //add data into pivot table
        $user->branches()->attach($request->input('branches'));
        $user->role()->attach($request->input('role'));

        return redirect('/admin/users/')->with('success', 'User created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $roles = Role::pluck('name','id')->all();

        $branches = Branch::pluck('name', 'id')->all();

        return view('admin.users.edit', compact('user','roles','branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
//        dd($request->all());

        //dd($input);

        $user = User::find($id);

        $user->update($request->all());

        //add data into pivot table
        $user->branches()->sync($request->get('branches'));

        $user->role()->sync($request->input('role'));

        return redirect('/admin/users/')->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();

        return redirect('/admin/users/')->with('danger', 'User has been deleted.');
    }

    public function status(Request $request, $id){

        $user = User::find($id);

        $user->update($request->all());

        return redirect('/admin/users/')->with('success', 'Status updated successfully');
    }
}
