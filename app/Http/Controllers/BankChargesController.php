<?php

namespace App\Http\Controllers;

use App\Bank;
use App\BankCharge;
use App\BankChargeDetail;
use App\Branch;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BankChargesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
//        $this->middleware('user');
        $this->middleware('admin');
    }

    public function index()
    {
        $charges = BankChargeDetail::where('created_by', Auth::user()->id)->get();
//        dd($charges->trans_id);
        return view('BankCharges.index', compact('charges'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branch = Auth::user()->branches->first()->id;
        $branch_name = Auth::user()->branches->first()->name;
//        dd($branch_name);

        $branchBank = Branch::find($branch)->banks->pluck('id');

        $banks = Bank::whereIn('id', $branchBank)->get();

//        dd($banks);

        return view('BankCharges.create',compact('branch','banks','branch_name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $trans_date = Carbon::parse($request->trans_date)->toDateString();
        $dd = str_replace('-', '', $trans_date);
        $year = substr($dd,0,4);
        $month = substr($dd,4,2);
        $day = substr($dd,6,2);
        $user_id = Auth::user()->id;
        $trans_sl = str_pad(BankChargeDetail::count()+1, 6, '0', STR_PAD_LEFT);
        $trans_id = substr($request->branch_name,0,3).'-'.$dd.'-'.(BankChargeDetail::count()+1);


        $total = 0;
        foreach ($request->bank as $key=>$value){
            $bank_id = $key;
            $amount = $value;
            $bank_name = Bank::find($bank_id)->short_name;
            //dd($user_id);

            $summary = new BankCharge;
            $summary->trans_id = $trans_id;
            $summary->trans_sl = $trans_sl;
            $summary->bank_id = $bank_id;
            $summary->bank_name = $bank_name;
            $summary->amount = $amount;
            $summary->save();
            $total = $total+$amount;
//            dd($total);

        }
        $detail = new BankChargeDetail;

        $detail->trans_id = $trans_id;
        $detail->trans_sl = $trans_sl;
        $detail->trans_date = $trans_date;
        $detail->year = $year;
        $detail->month = $month;
        $detail->day = $day;
        $detail->total = $total;
        $detail->branch_id = $request->branch;
        $detail->branch_name = $request->branch_name;
        $detail->created_by = $user_id;

        $detail->save();

        return redirect('/bankcharges')->with('success', 'Bank Charges Created Successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank_trans = BankChargeDetail::findOrFail($id);

        return view('BankCharges.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
