<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'f_name', 'l_name','c_name', 'username', 'email', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->belongsToMany('App\Role');
    }
    public function branches(){
        return $this->belongsToMany('App\Branch','user_branch');
    }
    //lists of branch id associated with current user
//    public function getBranchListAttribute(){
//        return $this->branches->pluck('id')->all();
//    }
}
