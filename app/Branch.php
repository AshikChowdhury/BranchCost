<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['br_code', 'status', 'name', 'description', 'address'];

    public function banks(){
        return $this->belongsToMany('App\Bank');
    }

    public function products(){
        return $this->belongsToMany('App\Product','product_branch');
    }
}
