<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_charges_summary', function (Blueprint $table) {
            $table->increments('id');
            $table->string('trans_id')->nullable();
            $table->string('trans_sl')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('bank_name')->nullable();
            $table->integer('amount');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_charges_summary');
    }
}
