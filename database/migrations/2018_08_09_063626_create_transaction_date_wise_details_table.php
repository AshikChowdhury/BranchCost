<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionDateWiseDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions_date_wise_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('trans_id')->comment = "Date wise receipt head id";
            $table->string('trans_sl')->comment = "Date wise receipt head serial number";
            $table->integer('branch_id')->nullable();
            $table->string('branch_name')->nullable();
            $table->string('trans_date')->nullable();
            $table->integer('brand_id')->nullable();
            $table->string('brand_name')->nullable();
            $table->integer('product_id')->nullable();
            $table->string('product_name')->nullable();
            $table->string('collection')->nullable();
            $table->string('money_receipt')->nullable();
            $table->string('adv_receipt')->nullable();
            $table->string('short_receipt')->nullable()->comment = "short/express receipt head id";;
            $table->string('adv_adj')->nullable()->comment = "advance adjustment";
            $table->string('adv_plus')->nullable()->comment = "adjustment(+)";
            $table->string('adv_minus')->nullable()->comment = "adjustment(-)";
            $table->integer('year');
            $table->string('month');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_by');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions_date_wise_details');
    }
}
