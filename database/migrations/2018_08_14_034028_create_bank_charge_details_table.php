<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankChargeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_charge_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('trans_id')->nullable();
            $table->string('trans_sl')->nullable();
            $table->date('trans_date');
            $table->string('year');
            $table->string('month');
            $table->string('day');
            $table->integer('branch_id')->nullable();
            $table->string('branch_name')->nullable();
            $table->integer('total')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_charge_details');
    }
}
