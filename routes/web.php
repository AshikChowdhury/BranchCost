<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout', 'Auth\LoginController@logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::patch('admin/branch/status/{branch}', 'BranchesController@status');

Route::resource('admin/branch', 'BranchesController');

Route::patch('admin/users/status/{user}', 'UsersController@status');

Route::resource('admin/users', 'UsersController');

Route::resource('admin/banks', 'BanksController');

Route::resource('admin/roles', 'RolesController');

Route::resource('admin/brands', 'BrandsController');

Route::patch('admin/products/status/{products}', 'ProductsController@status');

Route::resource('admin/products', 'ProductsController');

//Route::group(['middleware'=>['admin', 'user']], function () {
//    Route::resource('transactions', 'DateWiseTransactionsController');
//});
Route::resource('transactions', 'DateWiseTransactionsController');

Route::resource('bankcharges', 'BankChargesController');
