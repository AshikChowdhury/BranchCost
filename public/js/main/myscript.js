
$(document).ready(function() {
    // validate signup form on keyup and submit
    $("#UserForm").validate({
        rules: {
            f_name: "required",
            username: {
                required: true,
                minlength: 3
            },
            password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
            status: "required",
            branches: "required",
            role: "required"
        },
        messages: {
            f_name: "Please enter your first name",
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 3 characters"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password"
            },
            email: "Please enter a valid email address",
            status: "Please select status",
            branches: "Please select branches",
            role: "Please select role"
        }
    });
    $('#branches').select2();

    $('#banks').select2();

    $('#products').select2();

    $('#dataTables').DataTable({
        responsive: true,
        "columnDefs": [{
            "targets": [4,5], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
        // "order": []
    });

    $('#usertable').DataTable({
        responsive: true,
        "columnDefs": [{
            "targets": [5,6], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    $('#roletable').DataTable({
        responsive: true,
        "columnDefs": [{
            "targets": [0,3,4], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
        "order": []
    });
    $('#brandtable').DataTable({
        responsive: true,
        "columnDefs": [{
            "targets": [0,5,6], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
        "order": []
    });

    $('#producttable').DataTable({
        responsive: true,
        "columnDefs": [{
            "targets": [5,6], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    $('#banktable').DataTable({
        responsive: true,
        "columnDefs": [{
            "targets": [5], // column or columns numbers
            "orderable": false,  // set orderable for selected columns
        }],
    });

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
    });
    $('.datepicker').datepicker("setDate", new Date());


    $(".mytab").find("input").each(function(){
        $(this).keyup(function(){
            //alert("here")
            calculateSum();
        });
    });

    function calculateSum()
    {
        var sum=0;
        $(".mytab").each(function(){
            $(this).find('input[type="number"]').each(function()  {
                if(!isNaN(this.value)&& (this.value.length!=0))
                {
                    sum+=parseFloat(this.value);
                }
            });
        });
        $("#sum").html(sum.toFixed(2));

        // alert(sum);
    }

    // function calculateSum1(selector, brand)
    // {
    //     var sum=0;
    //     $(".mytab").each(function(){
    //         $(this).find('input[type="number"]').each(function()  {
    //             if(!isNaN(this.value)&& (this.value.length!=0))
    //             {
    //                 sum+=parseInt(this.value);
    //             }
    //         });
    //     });
    //     $("#sum").html(sum.toFixed(2));
    //
    //     alert(sum);
    // }
});



