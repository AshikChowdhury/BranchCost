<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Standard Format</title>

    <!-- Bootstrap Core CSS -->
    {{--<link href="/css/main/bootstrap.min.css" rel="stylesheet">--}}
    <link href="/css/main/s_bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/css/main/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/css/main/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    {{--<link href="/css/main/morris.css" rel="stylesheet">--}}

    <!-- Select2 CSS -->
    <link rel="stylesheet" href="/css/main/select2.min.css">

    <!-- Custom Fonts -->
    <link href="/css/main/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="/css/main/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/css/main/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Datepicker CSS -->
    <link href="/css/main/bootstrap-datepicker.min.css" rel="stylesheet">



</head>

<body>

<div id="wrapper">
    @include('includes.navbar')


    @yield('content')
</div>

<!-- jQuery -->
<script src="/js/main/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/js/main/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/js/main/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
{{--<script src="/js/main/raphael.min.js"></script>--}}
{{--<script src="/js/main/morris.min.js"></script>--}}
{{--<script src="/js/main/morris-data.js"></script>--}}

<!-- DataTables JavaScript -->
<script src="/js/main/datatables/js/jquery.dataTables.min.js"></script>
<script src="/js/main/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="/js/main/datatables-responsive/dataTables.responsive.js"></script>

<!--Jquery validator -->
<script src="/js/main/jquery.validate.min.js"></script>
<script src="/js/main/myscript.js"></script>

<!--Select2 for multi select -->
<script src="/js/main/select2.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="/js/main/sb-admin-2.js"></script>

<!-- Bootstrap date JavaScript -->
<script src="/js/main/bootstrap-datepicker.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment-with-locales.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js"></script>
<script>
    Chart.defaults.global.responsive      = true;
    Chart.defaults.global.scaleFontFamily = "'Source Sans Pro'";
    Chart.defaults.global.animationEasing = "easeOutQuart";
</script>

</body>

</html>
