@extends('layouts.app')

@section('content')

    <div id="page-wrapper">
        <style>
            .modal-confirm {
                color: #636363;
                width: 400px;
            }
            .modal-confirm .modal-content {
                padding: 20px;
                border-radius: 5px;
                border: none;
                text-align: center;
                font-size: 14px;
            }
            .modal-confirm .modal-header {
                border-bottom: none;
                position: relative;
            }
            .modal-confirm h4 {
                text-align: center;
                font-size: 26px;
                margin: 30px 0 -10px;
            }
            .modal-confirm .close {
                position: absolute;
                top: -5px;
                right: -2px;
            }
            .modal-confirm .modal-body {
                color: #000000;
            }
            .modal-confirm .modal-footer {
                border: none;
                text-align: center;
                border-radius: 5px;
                font-size: 13px;
                padding: 10px 15px 25px;
            }
            .modal-confirm .modal-footer a {
                color: #999;
            }
            .modal-confirm .icon-box {
                width: 80px;
                height: 80px;
                margin: 0 auto;
                border-radius: 50%;
                z-index: 9;
                text-align: center;
                border: 3px solid #f15e5e;
            }
            .modal-confirm .icon-box i {
                color: #f15e5e;
                font-size: 46px;
                display: inline-block;
                margin-top: 13px;
            }
            #table .form-control{
                height: 25px;
            }
            .require:after{
                content:'*';
                color:red;
            }
            input[type=number]{
                text-align: right;
            }

        </style>
        <div class="row" style="padding-top: 50px">
            <div class="col-lg-8 col-lg-offset-2">
                @include('includes.messages')
                <h4><strong>Bank Charges</strong></h4>
                <div class="panel-heading col-lg-3">
                    {!! Form::open(['method'=>'POST', 'action'=>'BankChargesController@store']) !!}
                    {!! Form::label('', 'Date', ['class'=>'require']) !!}
                    {!! Form::text('trans_date', null, ['class'=>'form-control datepicker', 'id'=>'date','required', 'placeholder'=>'DD/MM/YYYY']) !!}
                </div>
                <div class="col-lg-9">
                    <div class="panel-heading pull-right">
                        {!! Form::label('', 'Branch') !!}
                        {!! Form::hidden('branch', $branch) !!}
                        {!! Form::hidden('branch_name', $branch_name) !!}
                        {!! Form::text('', $branch_name, ['class'=>'form-control', 'disabled']) !!}
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="table">
                            <thead>
                            <tr class="info">
                                <th>SL</th>
                                <th>Bank Name</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($banks as $bank)
                            <tr class="info">
                                <th class="col-lg-1">{{$loop->iteration}}</th>
                                <td class="col-lg-2">{{$bank->short_name}}</td>
                                <td class="col-lg-2">{!! Form::number('bank['.$bank->id.'],[amount]', null, ['class'=>'form-control']) !!}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                        <div class="col-lg-12 text-center">
                            {{ Form::button('<i class="fa fa-check"></i> Save', ['type' => 'submit', 'class' => 'btn btn-primary'] ) }}
                            <a href="" class="btn btn-warning"><i class="fa fa-reply"></i> Cancel</a>
                        </div>
                    </div>

                {!! Form::close() !!}
                <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
@endsection