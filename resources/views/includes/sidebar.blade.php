<style>
    .active{
        color: #0905ff;
    }
</style>
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                </div>
                <!-- /input-group -->
            </li>
            <li class="{{Request::is('branch.index') ? 'active' : ''}}">
                <a href="/home"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-cogs fa-fw"></i> Master Setup<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{Request::is('branch.index') ? 'active' : ''}}">
                        <a href="{{route('branch.index')}}"><i class="fa fa-home"></i> Branch</a>
                    </li>
                    <li class="{{Request::is('users.index') ? 'active' : ''}}">
                        <a href="{{route('users.index')}}"><i class="fa fa-users"></i> Users</a>
                    </li>
                    <li class="{{Request::is('banks.index') ? 'active' : ''}}">
                        <a href="{{route('banks.index')}}"><i class="fa fa-bank"></i> Banks</a>
                    </li>
                    <li class="{{Request::is('role.index') ? 'active' : ''}}">
                        <a href="{{route('roles.index')}}"><i class="fa fa-user"></i> Roles</a>
                    </li>
                    <li class="{{Request::is('brands.index') ? 'active' : ''}}">
                        <a href="{{route('brands.index')}}"><i class="fa fa-first-order"></i> Brands</a>
                    </li>
                    <li class="{{Request::is('products.index') ? 'active' : ''}}">
                        <a href="{{route('products.index')}}"><i class="fa fa-product-hunt"></i> Products</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-money fa-fw"></i> Transaction<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{Request::is('transactions.create') ? 'active' : ''}}">
                        <a href="{{route('transactions.create')}}"><i class="fa fa-money"></i> Create</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-archive fa-fw"></i> Bank Charges<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{Request::is('bankcharges.index') ? 'active' : ''}}">
                        <a href="{{route('bankcharges.index')}}"><i class="fa fa-money"></i> Create</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href=""><i class="fa fa-bell fa-fw"></i> Log Viewer<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{Request::is('log-viewer.index') ? 'active' : ''}}">
                        <a href="{{url('log-viewer')}}"><i class="fa fa-eye"></i> Dashboard</a>
                    </li>
                    <li class="{{Request::is('log-viewer.logs') ? 'active' : ''}}">
                        <a href="{{url('log-viewer/logs')}}"><i class="fa fa-caret-square-o-right"></i> Logs</a>
                    </li>
                </ul>
            </li>
            {{--<li>--}}
                {{--<a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>--}}
                {{--<ul class="nav nav-second-level">--}}
                    {{--<li>--}}
                        {{--<a href="#">Panels and Wells</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="#">Buttons</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="#">Notifications</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="#">Typography</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
                {{--<!-- /.nav-second-level -->--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>--}}
                {{--<ul class="nav nav-second-level">--}}
                    {{--<li>--}}
                        {{--<a href="#">Second Level Item</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="#">Second Level Item</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="#">Third Level <span class="fa arrow"></span></a>--}}
                        {{--<ul class="nav nav-third-level">--}}
                            {{--<li>--}}
                                {{--<a href="#">Third Level Item</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#">Third Level Item</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                        {{--<!-- /.nav-third-level -->--}}
                    {{--</li>--}}
                {{--</ul>--}}
                {{--<!-- /.nav-second-level -->--}}
            {{--</li>--}}
            <li>
                <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">Blank Page</a>
                    </li>
                    <li>
                        <a href="#">Login Page</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>