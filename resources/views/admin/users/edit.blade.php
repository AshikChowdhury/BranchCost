@extends('layouts.app')

@section('content')

    <div id="page-wrapper">
        <style>
            .error{
                color: red;
            }
            .require:after{
                content:'*';
                color:red;
            }
        </style>
        <div class="row" style="padding-top: 50px; padding-bottom: 50px">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><strong>Edit User</strong></h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                {!! Form::model($user,['method'=>'PUT','id'=>'UserForm', 'action'=>['UsersController@update',$user->id]]) !!}
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'First Name ', ['class' => 'require']) !!}
                                        {!! Form::text('f_name', null, ['class'=>'form-control','name'=>'f_name', 'placeholder'=>'First Name']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Email ',['class' => 'require']) !!}
                                        {!! Form::email('email', null, ['class'=>'form-control','name'=>'email','type'=>'email', 'placeholder'=>'Email']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Status ', ['class' => 'require']) !!}
                                        {!! Form::select('status', [1=>'Active',0=>'Inactive'], 1, ['class'=>'form-control', 'name'=>'status']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Branches ', ['class' => 'require']) !!}
                                        {!! Form::select('branches[]', $branches, $user->branches->pluck('id') , ['class' => 'form-control','id' => 'branches','required','multiple'=>"multiple"]) !!}
                                    </div>

                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        {!! Form::label('', 'Last Name') !!}
                                        {!! Form::text('l_name', null, ['class'=>'form-control','name'=>'l_name', 'placeholder'=>'Last Name']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'User Name ', ['class' => 'require']) !!}
                                        {!! Form::text('username', null, ['class'=>'form-control', 'placeholder'=>'User Name', 'name'=>'username']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Role ',['class' => 'require']) !!}
                                        {!! Form::select('role_id', ['' => 'Choose Role'] + $roles , $user->role->pluck('id'), ['class'=>'form-control','name'=>'role']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Company Name ') !!}
                                        {!! Form::text('c_name', null, ['class'=>'form-control', 'placeholder'=>'Company Name', 'name'=>'c_name']) !!}
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="text-center">
                                        {{ Form::button('<i class="fa fa-check"></i> Update', ['type' => 'submit', 'class' => 'btn btn-primary'] ) }}
                                        <a href="{{route('users.index')}}" class="btn btn-warning"><i class="fa fa-reply"></i> Cancel</a>
                                    </div>
                                </div>

                                {!! Form::close() !!}
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

@endsection