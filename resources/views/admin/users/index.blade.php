@extends('layouts.app')

@section('content')

    <div id="page-wrapper">
        <style>
            .modal-confirm {
                color: #636363;
                width: 400px;
            }
            .modal-confirm .modal-content {
                padding: 20px;
                border-radius: 5px;
                border: none;
                text-align: center;
                font-size: 14px;
            }
            .modal-confirm .modal-header {
                border-bottom: none;
                position: relative;
            }
            .modal-confirm h4 {
                text-align: center;
                font-size: 26px;
                margin: 30px 0 -10px;
            }
            .modal-confirm .close {
                position: absolute;
                top: -5px;
                right: -2px;
            }
            .modal-confirm .modal-body {
                color: #000000;
            }
            .modal-confirm .modal-footer {
                border: none;
                text-align: center;
                border-radius: 5px;
                font-size: 13px;
                padding: 10px 15px 25px;
            }
            .modal-confirm .modal-footer a {
                color: #999;
            }
            .modal-confirm .icon-box {
                width: 80px;
                height: 80px;
                margin: 0 auto;
                border-radius: 50%;
                z-index: 9;
                text-align: center;
                border: 3px solid #f15e5e;
            }
            .modal-confirm .icon-box i {
                color: #f15e5e;
                font-size: 46px;
                display: inline-block;
                margin-top: 13px;
            }

        </style>
        <div class="row" style="padding-top: 50px">
            <div class="col-lg-12">
                @include('includes.messages')
                <div class="col-lg-4">
                    <h4><strong>All Users</strong></h4>
                </div>
                <div class="col-lg-8">
                    <div class="panel-heading pull-right">
                        <a href="{{route('users.index')}}" class="btn-default btn-sm"><i class="fa fa-server"></i> All Users</a>
                        <a href="{{route('users.create')}}" class="btn-primary btn-sm"><i class="fa fa-plus"></i> Add New</a>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="usertable">
                            <thead>
                            <tr class="danger">
                                <th>#</th>
                                <th>First Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th width="45px">Status</th>
                                <th width="110px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr class="success">
                                    <td>{{$user->id}}</td>
                                    <td>{{$user->f_name}}</td>
                                    <td>{{$user->username}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{isset($user->role->first()->name) ? ucfirst($user->role->first()->name): "Not Found"}}</td>
                                    <td class="text-center">
                                        @if($user->status == 1)
                                            {!! Form::open(['method'=>'PATCH', 'action'=>['UsersController@status', $user->id]]) !!}
                                            <input type="hidden" name="status" value="0">
                                            {{ Form::button('Active', ['type' => 'submit', 'class' => 'btn btn-success btn-xs'])}}
                                            {!! Form::close() !!}
                                        @else
                                            {!! Form::open(['method'=>'PATCH', 'action'=>['UsersController@status', $user->id]]) !!}
                                            <input type="hidden" name="status" value="1">
                                            {{ Form::button('Inactive', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs'] ) }}
                                            {!! Form::close() !!}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('users.show', $user->id)}}" class="btn btn-primary btn-xs">View</a>
                                        <a href="{{route('users.edit', $user->id)}}" class="btn btn-info btn-xs">Edit</a>
                                        <button href="#myModal" class="btn-xs btn-danger" data-toggle="modal">Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>

    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="fa fa-times-circle"></i>
                    </div>
                    <h4 class="modal-title">Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <button style="border: none; background: none">
                        {!! Form::open(['method'=>'DELETE', 'action'=>['UsersController@destroy', $user->id]]) !!}
                        {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection