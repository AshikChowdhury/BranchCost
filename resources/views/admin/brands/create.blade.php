@extends('layouts.app')

@section('content')

    <div id="page-wrapper">
        <style>
            .require:after{
                content:'*';
                color:red;
            }
        </style>
        <div class="row" style="padding-top: 50px; padding-bottom: 50px">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><strong>Create New Brand</strong></h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12 col-lg-offset-1">
                                {!! Form::open(['method'=>'POST', 'action'=>'BrandsController@store']) !!}
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        {!! Form::label('', 'Code') !!}
                                        {!! Form::text('code', null, ['class'=>'form-control', 'placeholder'=>'Brand Code']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Name ',['class' => 'require']) !!}
                                        {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Brand Name', 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        {!! Form::label('', 'Order ',['class' => 'require']) !!}
                                        {!! Form::number('order', null, ['class'=>'form-control', 'placeholder'=>'Order', 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Remark') !!}
                                        {!! Form::text('remark', null, ['class'=>'form-control','placeholder'=>'Remark']) !!}
                                    </div>
                                </div>
                                <div class="col-lg-10">
                                    <div class="text-center">
                                        {{ Form::button('<i class="fa fa-check"></i> Save', ['type' => 'submit', 'class' => 'btn btn-primary'] ) }}
                                        <a href="{{route('brands.index')}}" class="btn btn-warning"><i class="fa fa-reply"></i> Cancel</a>
                                    </div>
                                </div>

                                {!! Form::close() !!}
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

@endsection