@extends('layouts.app')

@section('content')

    <div id="page-wrapper">
        <style>
            .error{
                color: red;
            }
            .require:after{
                content:'*';
                color:red;
            }
        </style>
        <div class="row" style="padding-top: 50px; padding-bottom: 50px">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><strong>Create New Bank</strong></h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3">
                                {!! Form::open(['method'=>'POST', 'action'=>'BanksController@store']) !!}
                                <div class="form-group">
                                    {!! Form::label('', 'Bank Name ',['class' => 'require']) !!}
                                    {!! Form::text('bank_name', null, ['class'=>'form-control', 'placeholder'=>'Ex: Prime Bank Limited','required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('', 'Short Name ',['class' => 'require']) !!}
                                    {!! Form::text('short_name', null, ['class'=>'form-control', 'placeholder'=>'Ex: PBL', 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('', 'Address ') !!}
                                    {!! Form::textarea('address', null, ['class'=>'form-control','rows'=>2, 'placeholder'=>'Ex: Gulshan 2']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('', 'Remark ',['class' => 'require']) !!}
                                    {!! Form::text('remark', null, ['class'=>'form-control','placeholder'=>'Ex: Remark', 'required']) !!}
                                </div>
                                <div class="text-center">
                                    {{ Form::button('<i class="fa fa-check"></i> Save', ['type' => 'submit', 'class' => 'btn btn-primary'] ) }}
                                    <a href="{{route('banks.index')}}" class="btn btn-warning"><i class="fa fa-reply"></i> Cancel</a>
                                </div>

                                {!! Form::close() !!}
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

@endsection