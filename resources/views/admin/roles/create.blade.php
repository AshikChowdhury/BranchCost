@extends('layouts.app')

@section('content')

    <div id="page-wrapper">
        <style>
            .require:after{
                content:'*';
                color:red;
            }
        </style>
        <div class="row" style="padding-top: 50px; padding-bottom: 50px">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><strong>Create New Role</strong></h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3">
                                {!! Form::open(['method'=>'POST', 'action'=>'RolesController@store']) !!}
                                <div class="form-group">
                                    {!! Form::label('', 'Name ',['class' => 'require']) !!}
                                    {!! Form::text('name', null, ['class'=>'form-control', 'required', 'placeholder'=>'Ex: Executive']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('', 'Sort ',['class' => 'require']) !!}
                                    {!! Form::number('sort', null, ['class'=>'form-control', 'required','placeholder'=>'Ex: 1']) !!}
                                </div>
                                <div class="text-center">
                                    {{ Form::button('<i class="fa fa-check"></i> Save', ['type' => 'submit', 'class' => 'btn btn-primary'] ) }}
                                    <a href="{{route('roles.index')}}" class="btn btn-warning"><i class="fa fa-reply"></i> Cancel</a>
                                </div>

                                {!! Form::close() !!}
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

@endsection