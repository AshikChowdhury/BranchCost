@extends('layouts.app')

@section('content')

    <div id="page-wrapper">
        <style>
            .require:after{
                content:'*';
                color:red;
            }
        </style>
        <div class="row" style="padding-top: 50px; padding-bottom: 50px">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><strong>Edit Branch</strong></h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1">
                                {!! Form::model($branch,['method'=>'PUT', 'action'=>['BranchesController@update', $branch->id]]) !!}
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        {!! Form::label('', 'Code') !!}
                                        {!! Form::text('br_code', null, ['class'=>'form-control', 'placeholder'=>'Branch Code']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Name ',['class' => 'require']) !!}
                                        {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Branch Name', 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        {!! Form::label('', 'Description ',['class' => 'require']) !!}
                                        {!! Form::text('description', null, ['class'=>'form-control', 'placeholder'=>'Description', 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Address') !!}
                                        {!! Form::textarea('address', null, ['class'=>'form-control', 'rows'=>1, 'placeholder'=>'Address']) !!}
                                    </div>
                                </div>
                                <div class="col-lg-10">
                                    <div class="form-group">
                                        {!! Form::label('', 'Banks ', ['class' => 'require']) !!}
                                        {!! Form::select('banks[]', $banks, $branch->banks->pluck('id'), ['class' => 'form-control','id' => 'banks','required','multiple'=>"multiple"]) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Products ', ['class' => 'require']) !!}
                                        {!! Form::select('products[]', [0=>'All']+$products, $branch->products->pluck('id'), ['class' => 'form-control','id' => 'products','required','multiple'=>"multiple"]) !!}
                                    </div>
                                </div>
                                <div class="col-md-8 text-center">
                                    {{ Form::button('<i class="fa fa-check"></i> Update', ['type' => 'submit', 'class' => 'btn btn-primary'] ) }}
                                    <a href="{{route('branch.index')}}" class="btn btn-warning"><i class="fa fa-reply"></i> Cancel</a>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

@endsection