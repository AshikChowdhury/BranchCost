@extends('layouts.app')

@section('content')

    <div id="page-wrapper">
        <style>
            .require:after{
                content:'*';
                color:red;
            }
        </style>
        <div class="row" style="padding-top: 50px; padding-bottom: 50px">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><strong>Edit Product</strong></h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12 col-lg-offset-1">
                                {!! Form::model($product,['method'=>'PATCH', 'action'=>['ProductsController@update',$product->id]]) !!}
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        {!! Form::label('', 'Code') !!}
                                        {!! Form::text('code', null, ['class'=>'form-control', 'placeholder'=>'Product Code']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Name ',['class' => 'require']) !!}
                                        {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Product Name', 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Short Name ',['class' => 'require']) !!}
                                        {!! Form::text('short_name', null, ['class'=>'form-control', 'placeholder'=>'Product Short Name', 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        {!! Form::label('', 'Brand ',['class' => 'require']) !!}
                                        {!! Form::select('brand_id', ['' => 'Choose Brand'] + $brands , $product->brand->pluck('id'), ['class'=>'form-control','required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Order ',['class' => 'require']) !!}
                                        {!! Form::number('order', null, ['class'=>'form-control', 'placeholder'=>'Order', 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('', 'Remark') !!}
                                        {!! Form::text('remark', null, ['class'=>'form-control','placeholder'=>'Remark']) !!}
                                    </div>
                                </div>
                                <div class="col-lg-10">
                                    <div class="text-center">
                                        {{ Form::button('<i class="fa fa-check"></i> Save', ['type' => 'submit', 'class' => 'btn btn-primary'] ) }}
                                        <a href="{{route('products.index')}}" class="btn btn-warning"><i class="fa fa-reply"></i> Cancel</a>
                                    </div>
                                </div>

                                {!! Form::close() !!}
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

@endsection